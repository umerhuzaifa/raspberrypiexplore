
#include<stdio.h>
#include<stdlib.h>

#define NODEMAX 10

typedef void fun_ptr(int); // a new type is defined for the listeners

struct node{
	int val;
	struct node* next;
	int ID; 
	fun_ptr* cbFunc;
};


typedef struct node node_t;

node_t* head = NULL;
node_t* tail = NULL;

volatile static int counter;

void mathFun(int mon){
	printf("Someone was brought to mathFun using function pointer \n");
	return;
}

void addNode(int value, fun_ptr* func){
	counter++;
	if (head == NULL){
		head=malloc(sizeof(node_t));		
		head->val = value;
		head->next = NULL;
		head->cbFunc = func;
		head->ID = counter;
		tail = head;
		
	}
	else{
		tail->next = malloc(sizeof(node_t));
		tail=tail->next;		
		tail->val = value;
		tail->next=NULL;
		tail->cbFunc = func;
		tail->ID = counter;
	}

	
	printf("%d value assigned to node # %d \n", value, counter);
	
	if (func!=NULL)
		(*(tail->cbFunc))(23);
	// (*(element->listener))();
}

void dispNode(node_t* ind_node){
	
	if (ind_node==NULL){
		printf("Nothing to show");
	}
	else{		
		counter--;		
		printf("Here is the node # %d : %d \n", ind_node->ID, ind_node->val);
		
		if (ind_node->next!=NULL){
			ind_node=ind_node->next;
			dispNode(ind_node);
		}
	}	
}
///////////////////////////////////////////////////
///// Pre Lab 6 - Q3a //////////////////////////////
node_t* get_last_node(node_t* startNode){
	
	if (startNode->next!=NULL){
		get_last_node(startNode->next);
	}
	else
		return startNode;
	
}
///// Pre Lab 6 - Q3b //////////////////////////////
void insert_after_node(node_t* prevNode, node_t* new_node){
	
	if (new_node!=NULL){
		prevNode->next 		= new_node;			
	}
}

void list_print(){
	node_t* node = head;
	while(node!=NULL)
	{
		printf("[%ld] ->", node->val);
		node = node->next;
	}
	printf("\n");
}
///////////////////////////////////////////////////
void main()
{
	counter = 0;

	while(counter<NODEMAX){
		addNode(rand(), &mathFun);
		list_print();
	}
	
	addNode(23, &mathFun);

	
	dispNode(head);

	fun_ptr* pfunc = &mathFun;

	(*pfunc)(100);
	list_print();


	// node_t* tmp;
	// while(head!=NULL){
 //    tmp = head;
 //    head = head->next;
 //    free(tmp);
 //  }
}
