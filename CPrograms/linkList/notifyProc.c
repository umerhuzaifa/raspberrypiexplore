//** Code: Motivating example for generating signals in the program (Fig. 11.2)
//** Author: Umer Huzaifa
//** Date: 12/25/2021
//** Comments: Played around with pointers calling functions with and without arguments
//**           Timestamp creation in the output

// #include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
// Some global variables and a new data type to access 
// listener functions in the program

static float fnum;   // a number for doing playful math operations with
volatile static int counter; 	   // a variable to keep track of the listeners added
volatile static time_t init_time;   

typedef void notifyProcedure(void); // a new type is defined for the listeners

// The list of listeners has special structure for the nodes
struct element{
	notifyProcedure* listener; // part of it is a pointer to a function
	struct element* next; 		// second part is a pointer to another similar node
};

// Definition for another type for the listener node

typedef struct element element_t;

// Pointers for keeping track of start and end of the listener list

element_t* head = 0;
element_t* tail = 0;

// A listener is added.
// If it is in the start, add a node from the head,
// Otherwise, add a node to the current tail.

void addListener(notifyProcedure* listener_fun){
	if (head==0){
		head = malloc(sizeof(element_t));
		head->listener=listener_fun;
		head->next=0;
		tail=head;
	}
	else{
		tail->next=malloc(sizeof(element_t));
		tail=tail->next;
		tail->listener=listener_fun;
		tail->next=0;
	}
	counter++;
}

// A function to inform the addition of the listener,
// involves traversing through each node and running their listeners

void update(){	
	element_t* element=head;

	while(element !=0){
		(*(element->listener))();
		element=element->next;
	}
}

void print(){
	fnum+=1;
	printf("Listeners added are: %d \t", counter);
	printf("Fun math is: %5.5f \n", fnum);
}


int main(){
	fnum = 0.25;
	init_time = time(NULL);
	float tstamp = 0;
	while(tstamp<.001){       // Run for .5 ms
		addListener(&print);
		update();
		tstamp = (float) (time(NULL)-init_time)/1000.0F;
		printf("timestamp: %f \t \n \n", tstamp);
	}
	return 0;
}