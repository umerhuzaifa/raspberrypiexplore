#include <stdio.h>
typedef struct
{
   int age;
   float weight;
   int * ptr;
}person;

int main()
{
    struct person *personPtr, person1;
    personPtr = &person1;   // personPtr is a person data type pointer
                            // and points to a person structure person1

    printf("Enter age: ");
    scanf("%d", &personPtr->age);

    printf("Enter weight: ");
    scanf("%f", &personPtr->weight);

    printf("Displaying:\n");
    printf("Age: %d\n", personPtr->age);
    printf("weight: %f", personPtr->weight);

    return 0;
}