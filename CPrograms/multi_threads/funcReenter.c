// Code: For Testing Creation and Functions of Threading in Linux (Fig. 11.4)
// Author: Umer Huzaifa
// Date: 12/29/2021
// Comments: Testing if static_data_func() can be called by multiple threads without issue
// My expectation is to see it fall apart because of memory inconsistency between two 
// threads.


#include<stdio.h>
#include<math.h>
#include<pthread.h>

volatile static float fnum;
volatile static int count;
pthread_t threadID1, threadID2;

void static_data_func(int * data){

  static int * code_cpy;
  code_cpy = data;
  
  count++;
  *code_cpy +=count;
  printf("Coming after running the function  %d time(s): ", count);
  printf("%d\n", *code_cpy);
}

void* printN(void* arg){
  int code = 623;
  
  printf("Welcome to Thread: %d \n", *((int *) arg));
 

  static_data_func(&code);

  printf("Updated variable from Thread %d is: %d \n", *((int *) arg), code);
  return NULL;
}

int main(){
  count = 0;
  fnum=0.5;
  printf("Welcome to Threading Use in Multiple Functions \n");
  time_t ref_time;
  ref_time = time(NULL);

    void* exitStatus;
    int x1 = 1, x2 = 2;

    pthread_create(&threadID1, NULL, printN, &x1);
        
    pthread_create(&threadID2, NULL, printN, &x2);
    
    // unless there are join thread commands, the threads may be
    // unresponsive

  printf("Started Threads \n");
  pthread_join(threadID1, &exitStatus);
  
  if (exitStatus==PTHREAD_CANCELED)
    printf("Thread with ID %d was cancelled. \n", (int)threadID1);

  // in case you want to ensure a non-interleaving execution of threads
  // while ((time(0) - ref_time)<2);
  pthread_join(threadID2, &exitStatus);

  if (exitStatus==PTHREAD_CANCELED)
    printf("Thread with ID %d was cancelled. \n", (int)threadID2);
 
  printf("Exit Status: %d \n", (int)exitStatus);    
  return 0;


}