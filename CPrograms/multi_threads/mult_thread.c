
// Code: For creating arbitrarily large number of threads (derived from Fig. 11.4)
// Author: Umer Huzaifa
// Date: 12/25/2021
// Comments: Multiple thread creation for a suitable arithematic task. 
// Each thread created decrements a given floating point number. The number should keep
// decreasing until either a threshold on the number is met or the desired number of created
// threads is met.


#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<pthread.h>

volatile static float fnum;
volatile static int counter;
static pthread_t threadID;


void* mathFun(void* arg){
	
	fnum*=1.00001;
	counter+=1;
	printf("Thread: %s - Math Result: %f \n", arg, fnum);

}


void* printN(void* arg){
	
	mathFun(arg);
	printf("Currently on Thread number %d using Thread ID: %s \n", counter, arg);
	return NULL;
	
}


int main(){

	fnum=1.5;
	counter= 0;
	printf("Welcome to Threading Use in Multiple Functions \n");
	time_t ref_time;
	ref_time = time(NULL);

    void* exitStatus;  
    char* thread_ID;

// -----------------------------------
// Experimental piece of code to decrement the number fnum with all the threads possible
// Each new thread is given a unique ID using rand() (coming from stdlib)

	printf("Started Threads \n");
    
	while(counter<=100){
    	threadID = (int)rand();
    	sprintf(thread_ID, "%d", threadID);
    	if(pthread_create(&threadID, NULL, printN, thread_ID)!=0){
    		perror("pthread_create() error");
    		exit(1);
    	}
    	
    	if(	pthread_join(threadID, &exitStatus) != 0){
    		perror("pthread_create() error");
    		exit(3);
    	}

    }
    return 0;
}





    // pthread_create(&threadID1, NULL, printN, &x1);
        
    // pthread_create(&threadID2, NULL, printN, &x2);
    
    // unless there are join thread commands, the threads may be
    // unresponsive

    
	// pthread_join(threadID1, &exitStatus);
	// if (exitStatus==PTHREAD_CANCELED)
	// 	printf("Thread with ID %d was cancelled. \n", (int)threadID1);

	// in case you want to ensure a non-interleaving execution of threads
	// while ((time(0) - ref_time)<2);
 //    pthread_join(threadID2, &exitStatus);

	// if (exitStatus==PTHREAD_CANCELED)
	// 	printf("Thread with ID %d was cancelled. \n", (int)threadID2);
 
 //    printf("Exit Status: %d \n", (int)exitStatus);    
 //    return 0;
