#include <stdio.h>


typedef struct
{
   int age;
   float weight;
   int * address;
}person;

int main()
{
    unsigned int NUM = 8;
    person* p1 = (person *) 0x0054;             // p1 is a person data type pointer
    // person* p1;
    
    p1->address = &NUM;

    *(p1->address) = 2*NUM;

    while(1)
    {
      // *(p1->address) = (NUM<<3) | (NUM >> 5);
      // printf("The rotated number is: %X \n", *(p1->address));
      // NUM = *(p1->address);
    }
    return 0;
}