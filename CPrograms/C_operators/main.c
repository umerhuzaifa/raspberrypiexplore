/* C++ Program to demonstrate use of left shift
operator */
#include<stdio.h>

#define INT_BITS 32
int main()
{
	// a = 5(00000101), b = 9(00001001)
	unsigned int a = 5, b = 9;    // make sure to use unsigned variable data type for avoiding the
								  // arithematic shifting

	int d = 1;    // number of bits to shift
	for (int i=0; i<=35; i++)
	{
		a = (a << d) | (a >> (INT_BITS - d));
	// The result is 00001010
	printf("a << %d = %X\n", d, a);
	
		b = (b >> d) | (b << (INT_BITS - d));
	// The result is 00010010
	// printf("b >> %d = %X\n", d, b);
}
	return 0;
}
