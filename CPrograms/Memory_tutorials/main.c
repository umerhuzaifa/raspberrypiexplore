#include<stdio.h>
#include<stdlib.h>
#include<stdint.h> // needed for uint8_t and others

char *chr_ptr;
char chr;
void main()
{
	int x;
	int *y; 
	uint8_t x8;
	uint16_t x16;
	
	y = &x;

	*y = 25;
	// chr_ptr = (int *) &x;
	// *chr_ptr = 30;
	// x holds the address of a specific location

	// *(int *) x = ;
	while(1){
		printf("Address held by y: %X -- address of x: %X \n", y, &x);	
		// printf("Value stored by x: %d \n", x);
		printf("Value stored by y: %X and value stored at x: %d \n", y, x);
		printf("The size of Char Pointer is %d \n", sizeof(chr_ptr));
		printf("The size of Char variable is %d \n", sizeof(chr));
		printf("Sizes of unsigned variable uint8_t is %d \n",sizeof(x8));
		printf("Sizes of unsigned variable uint16_t is %d \n",sizeof(x16));
		sleep(.500);
	}
}