// Code: For Testing Creation and Functions of Threading in Linux (Fig. 11.4)
// Author: Umer Huzaifa
// Date: 12/25/2021
// Comments: Played with introducing a time delay between thread creations. 
// This ensures the sequence of threads to be deterministic.


#include<stdio.h>
#include<math.h>
#include<pthread.h>

volatile static float fnum;
pthread_t threadID1, threadID2;

void* mathFun(volatile float* f, void* arg){
	float temp;

	temp = *(f);
	temp+=1;	

	*(f) = sqrtf(temp);
	printf("Thread: %d - Math Result: %f \n", *(int *) (arg), *(f));
}

void* printN(void* arg){
	int i;
	for (i=0; i<10; i++){
		printf("Repeated Runs with Thread ID: %d \n", *(int*) arg);
	}
	mathFun(&fnum, arg);
	return NULL;
	
}


int main(){
	
	fnum=0.5;
	printf("Welcome to Threading Use in Multiple Functions \n");
	time_t ref_time;
	ref_time = time(NULL);

    void* exitStatus;
    int x1 = 1, x2 = 2;

    pthread_create(&threadID1, NULL, printN, &x1);
        
    pthread_create(&threadID2, NULL, printN, &x2);
    
    // unless there are join thread commands, the threads may be
    // unresponsive

    printf("Started Threads \n");
	pthread_join(threadID1, &exitStatus);
	if (exitStatus==PTHREAD_CANCELED)
		printf("Thread with ID %d was cancelled. \n", (int)threadID1);

	// in case you want to ensure a non-interleaving execution of threads
	// while ((time(0) - ref_time)<2);
    pthread_join(threadID2, &exitStatus);

	if (exitStatus==PTHREAD_CANCELED)
		printf("Thread with ID %d was cancelled. \n", (int)threadID2);
 
    printf("Exit Status: %d \n", (int)exitStatus);    
    return 0;


}