


// Code: Testing Thread Creation and Running with Commonly Made Mistakes (Example 11.5)
// Author: Umer Huzaifa
// Date: 12/26/2021
// Comments: Checking how threads behave with automatic variables


#include<stdio.h>
#include<math.h>
#include<pthread.h>

volatile static float fnum;
pthread_t threadID1, threadID2;

void* mathFun(volatile float* f, void* arg){
	

	
	*(f)+=1;	

	*(f) = sqrtf(*(f));
	printf("Thread: %d - Math Result: %f \n", *(int *) (arg), *(f));
}

void* printN(void* arg){
	
	char *ret = (char *) malloc(10 *(sizeof(char))); // need to initialize with asome memory locations otherwise,
									 // strcpy copies the string to an unknown place
	printf("Thread Executed with Thread ID: %d \n", *(int*) arg);
	
	mathFun(&fnum, arg);

	strcpy(ret, "What a Ride!");
	pthread_exit(ret);

	return NULL;
	
}


int main(){
	
	fnum=rand()%100;
	printf("Welcome to Threading Use in Multiple Functions \n");
	time_t ref_time;
	ref_time = time(NULL);

    void* exitStatus;
    int x1 = 1, x2 = 2;

    pthread_create(&threadID1, NULL, printN, &x1);
        
    pthread_create(&threadID2, NULL, printN, &x2);
    
    // unless there are join thread commands, the threads may be
    // unresponsive

    printf("Started Threads \n");

	pthread_join(threadID1, &exitStatus);
	
	if (exitStatus==PTHREAD_CANCELED)
		printf("Thread with ID %d was cancelled. \n", (int)threadID1);

	printf("Exit Status: %s \n", exitStatus);    
	
    pthread_join(threadID2, &exitStatus);

	if (exitStatus==PTHREAD_CANCELED)
		printf("Thread with ID %d was cancelled. \n", (int)threadID2);
 	// sleep(5);
    printf("Exit Status: %s \n", exitStatus);    
    return 0;


}
