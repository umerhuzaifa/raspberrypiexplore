//** Code: Introducing mutex locks for different threads (Fig. 11.4)
//** Author: Umer Huzaifa
//** Date: 12/25/2021
//** Comments: 
//**           


#include<stdio.h>

#include<pthread.h>


void* printN(void* arg){
	int i;
	for (i=0; i<10; i++){
		printf("My ID: %d \n", *(int*) arg);
	}
	return NULL;
}

int main(){
	printf("Welcome \n");
	// pthread 
	pthread_t threadID1, threadID2;
    void* exitStatus;
    int x1 = 1, x2 = 2;
    pthread_create(ds&threadID1, NULL, printN, &x1);
    pthread_create(&threadID2, NULL, printN, &x2);
    
    // unless there are join thread commands, the threads may be
    // unresponsive

    printf("Started Threads \n");
	pthread_join(threadID1, &exitStatus);
    pthread_join(threadID2, &exitStatus);
    return 0;
}