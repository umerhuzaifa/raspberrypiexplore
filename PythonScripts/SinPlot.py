
import math
import matplotlib.pyplot as plt

t0= 0
tf = 10
Nstep = 500 
dt = (tf- t0)/Nstep
t = [i/dt for i in list(range(Nstep))]
y =[]
for i in range(Nstep):
	y.append(math.sin(t[i]))

plt.figure()
plt.plot(t, y)
plt.show()